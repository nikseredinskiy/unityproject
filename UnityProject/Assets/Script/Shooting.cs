﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

    public GameObject bullet;
    float bulletForce = 20f;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject theBullet = (GameObject) Instantiate(bullet, transform.position + new Vector3(0,0,0.1f), transform.rotation);
            theBullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletForce, ForceMode.Impulse);

            Destroy(theBullet, 2);
        }
	}
       
}
