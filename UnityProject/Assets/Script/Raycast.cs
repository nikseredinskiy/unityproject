﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Raycast : MonoBehaviour {

    public Text score;
    public int totalScore = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            Physics.Raycast(ray, out hit);
            Debug.DrawRay(transform.position, transform.forward, Color.red, 3);

            if ((hit.collider != null) && (hit.collider.tag == "Barrier"))
            {
                totalScore++;

                score.text = "Score: " + totalScore.ToString();
            }
        }
	}
}
