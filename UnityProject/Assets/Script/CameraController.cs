﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    public float speed;

	void Update () {
        float hAxis = Input.GetAxis("Horizontal") * speed;
        float yAxis = Input.GetAxis("Vertical") * speed;
        transform.position += new Vector3(hAxis, 0f, 0f);
        transform.position += new Vector3(0f, 0f, yAxis);
	}
}
