﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    public float speed;
	// Update is called once per frame
	void Update () {
        float hAxis = Input.GetAxis("Horizontal") * speed;
        float yAxis = Input.GetAxis("Vertical") * speed;
        transform.position += new Vector3(hAxis, 0f, 0f);
        transform.position += new Vector3(0f, 0f, yAxis);

	}
}
